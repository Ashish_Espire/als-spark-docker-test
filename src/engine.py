import os
from pyspark.mllib.recommendation import ALS
from pyspark.mllib.recommendation import MatrixFactorizationModel
 
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)



class RecommendationEngine:
    
    def getMovieIdTitleMap(self):
        movie_titles = self.movies_titles_RDD.collect()
        movie_dictionary = {}
        for movie in movie_titles:
            movie_dict = {str(movie[0]):movie[1]}
            movie_dictionary.update(movie_dict)
        return movie_dictionary
        
    def load_model(self):
        print("calling load_model...",self.dataset_path)
        model_path = os.path.join('./executor', 'models', 'model1')
        print("model_path : ",model_path)
        isExist = os.path.exists(model_path) 
        print("model_path exist : ********* ",model_path,isExist)
        if(isExist):
            imported_model = MatrixFactorizationModel.load(self.sc, model_path)
            return imported_model
            
    def get_ratings_for_movie_ids(self,user_id, movie_ids):
        #Given a user_id and a list of movie_ids, predict ratings for them
        requested_movies_RDD = self.sc.parallelize(movie_ids).map(lambda x: (user_id, x))
        # Get predicted ratings
        return self.predictAvarageRating(requested_movies_RDD)
        
    def get_top_ratings(self, user_id, movies_count):
        """Recommends up to movies_count top unrated movies to user_id
        """
        # Get pairs of (userID, movieID) for user_id unrated movies
        user_unrated_movies_RDD = self.movie_rdd.filter(lambda rating: not rating[1]==user_id).map(lambda x: (user_id, x[0]))
        # Get predicted ratings
        ratings = self.predictAvarageRating(user_unrated_movies_RDD)
        rating_rdd = self.sc.parallelize(ratings)
        print("rating_rdd ***** ",type(rating_rdd),rating_rdd.take(3))
        #filtered_rdd =  self.sc.parallelize(ratings).filter(lambda r: r[2]>=25).takeOrdered(movies_count, key=lambda x: -x[1])
        filtered_rdd =  rating_rdd.takeOrdered(movies_count, key=lambda x: -int(x['rating']))
        return filtered_rdd
    
    def predictAvarageRating(self,user_movie_rdd):
        model = self.load_model()
        predicted_RDD = model.predictAll(user_movie_rdd)
        predicted_rating_RDD = predicted_RDD.map(lambda x: (x.product, x.rating)).groupByKey()
        ratings = []
        for x in predicted_rating_RDD.collect():
            rating_average = sum(x[1])/len(x[1])
            dict = {"productId":str(x[0])+':'+self.movie_dictionary[str(x[0])],"rating":rating_average}
            ratings.append(dict)
        return ratings
        
        
    def pridict_rating(self):
        return self.predictAvarageRating(self.validation_for_predict_RDD) 
        

    
    def __init__(self, sc, dataset_path):
        """Init the recommendation engine given a Spark context and a dataset path
        """
 
        logger.info("Starting up the Recommendation Engine: ")
 
        self.sc = sc
        self.dataset_path =dataset_path
        rating_file = os.path.join(dataset_path, 'ratings.csv')
        movie_file = os.path.join(dataset_path, 'movies.csv')
        rating_data = sc.textFile(rating_file)
        movie_data = sc.textFile(movie_file)
        rating_data_header = rating_data.take(1)[0]
        movie_data_header = movie_data.take(1)[0]
        rating_data = rating_data.filter(lambda row : row!=rating_data_header).map(lambda line : line.split(','))\
        .map(lambda rating : (rating[0],rating[1],rating[2])).cache()
        self.ratings_RDD = rating_data
        self.movie_rdd = movie_data.filter(lambda row : row !=movie_data_header).map(lambda line : line.split(','))\
        .map(lambda movie : (movie[0],movie[1])).cache()
        movies_titles = self.movie_rdd.map(lambda x: (int(x[0]),x[1]))
        self.movies_titles_RDD = movies_titles
        #self.__count_and_average_ratings() ## this is throwing SPARK-5063 error, nested rdd not allowed in spark
        print("rating_data : ********* ",rating_data.take(3))
        training_RDD, test_RDD,validation_RDD = rating_data.randomSplit([7,2,1],seed=0)
        validation_for_predict_RDD = validation_RDD.map(lambda x : (x[0],x[1]))
        self.rating_data = rating_data
        self.validation_for_predict_RDD = validation_for_predict_RDD
        self.movie_dictionary = self.getMovieIdTitleMap()
        