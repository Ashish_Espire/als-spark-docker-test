from flask import Blueprint
main = Blueprint('main', __name__)
 
import json
from engine import RecommendationEngine
 
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
 
from flask import Flask, request
 
@main.route("/hello/<string:user_name>", methods = ["GET"])
def say_hello(user_name):
	message = "Hello, "+user_name
	print("Welcome to Cherrypy server ")
	return message

@main.route("/<int:user_id>/ratings/top/<int:count>", methods=["GET"])
def top_ratings(user_id, count):
    logger.debug("User %s TOP ratings requested", user_id)
    top_ratings = recommendation_engine.get_top_ratings(user_id,count)
    return json.dumps(top_ratings)
	
@main.route("/<int:user_id>/ratings/<int:movie_id>", methods = ["GET"])
def getMovieRatings(user_id, movie_id):
	logger.debug("User %s rating requested for movie %s", user_id, movie_id)
	ratings = recommendation_engine.get_ratings_for_movie_ids(user_id, [movie_id])
	return json.dumps(ratings)
	
@main.route("/pridiction", methods = ["GET"])
def pridict_rating():
	print("Welcome to pridiction ")
	return json.dumps(recommendation_engine.pridict_rating())
	
	
def create_app(spark_context, dataset_path):
    global recommendation_engine 
 
    recommendation_engine = RecommendationEngine(spark_context, dataset_path)    
    
    app = Flask(__name__)
    app.register_blueprint(main)
    return app