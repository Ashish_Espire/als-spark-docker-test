Some key command for docker

---------

1) Command to see docker version
   docker --version

2) Command to detailed information about docker (gives details of client and server)
   docker info

3) How to build a docker image (. in build image command is context of docker, the floder where Dockerfile resides)

	a) create a folder
	b) create a file titled Dockerfile (without any extension) in the floder
	c) docker build -t image_name .

4) How to deploy/run the image in docker container (-p map a port used in image/container to operating system port, 
we can also map volume with -v)

	docker container run [-p osport:imageport] image_name

5) How to list images and container

	docker images 

	docker container ls -about

6) How do delete/stop container (-f forcefully removed)

	docker container rm -f container_id
	docker container stop container_id

7) How do delete image
	docker image rm -f image_id

8) How to see the details (ip , port etc) used for the given container

	docker inspect container_id

9) Dokerfile script info

	a)copy all resource in src folder to executor
		COPY ./src ./executor
	b)confifure environment path
		ENV SPARK_HOME /usr/local/spark
		ENV PYTHONPATH $SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.10.7-src.zip
		ENV PATH $SPARK_HOME/bin:${SPARK_HOME}/jars/*:$PATH
	c) execute the server.py while runnin the docker container
		CMD ["spark-submit", "./executor/server.py"]

	

10)Project resource details

	a)src folder containes python files
		1)app.py is restendpoint implmentation
		2)server.py is used to start the cherrypy server
		3)engine.py it utility class for intializing deafult data, loading a saved model (located in models folder),get the pridiction
		on the movie dataset(given in datasets folder) using the saved model.
	
	b)executor folder is used by Dockerfile script to copy data from src folder to it, to be used in conationer execution context
	

11)how to run the project

	a) Open the cmd in the project folder laction (als-spark-docker-test , where the Dockerfile resides)
	b) Create the image : docker build -t pridiction .
	c) Deploy the image and run the container : docker container run -p 9090:9090 pridiction 
	d) Get the pridiction http://localhost:9090/pridiction
	e) Get top N rating of corrosponding user :	http://localhost:9090/{user_id}/ratings/top/{topN} : http://localhost:9090/4/ratings/top/10
	f) Get rating for given user_id and movie : 	http://localhost:9090/{user_id}/ratings/{movie_id} : http://localhost:9090/4/ratings/345




 

