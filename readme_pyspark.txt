Some pyspark details for loading an existing model, training (ALS) a model and data transformation

ALS : Apache Spark ML implements alternating least squares (ALS) for collaborative filtering, a popular algorithm for making recommendations.


1) How to create rdd from a csv file

from pyspark import SparkContext, SparkConf
from pyspark.mllib.recommendation import ALS
from pyspark.mllib.recommendation import MatrixFactorizationModel
import os

data_path = "./executor/datasets"
file_dir = "ml-latest-small"
dataset_path = os.path.join(data_path, file_dir)
rating_file = os.path.join(dataset_path, 'ratings.csv')
rating_rdd = sc.textFile(rating_file)

2) how to get header of an csv file

	rating_data_header = rating_rdd.take(1)[0]

3) how to filter out heading row from rating_rdd

	rating_data = rating_rdd.filter(lambda row : row!=rating_data_header).map(lambda line : line.split(','))\
        .map(lambda rating : (rating[0],rating[1],rating[2])).cache()
		
4) how to create get data for training,testing and validation form rating_rdd

	training_RDD, test_RDD,validation_RDD = rating_data.randomSplit([7,2,1],seed=0)
	
	Note : training_RDD will have 70% data,test_RDD have 20% data and  validation_RDD will have 10% (% are approx there may be sligh deviation)
	
5) how to load an exisitng model 

	model_path = os.path.join('./executor', 'models', 'model1')
	model = MatrixFactorizationModel.load(self.sc, model_path)
	
6) how to get pridiction
	validation_for_predict_RDD = validation_RDD.map(lambda x : (x[0],x[1]))
	predicted_RDD = model.predictAll(validation_for_predict_RDD)
	predicted_rating_RDD = predicted_RDD.map(lambda x: (x.product, x.rating)) #create tuple/rdd from movie_id and rating
	predicted_rating_RDD = predicted_rating_RDD.groupByKey() #group the rating based on movie id
	
7) how to train a model
	model = ALS.train(rating_data, rank=4, seed=0,
                               iterations=10, lambda_=0.1)
							   i
8) how to create a dictionay/map from an rdd

		movie_titles = self.movies_titles_RDD.collect()
        movie_dictionary = {} # movie_dictionary map will have movie_id as key and movie_title as value
        for movie in movie_titles:
            movie_dict = {str(movie[0]):movie[1]}
            movie_dictionary.update(movie_dict) # update the movie_dictionary with new dictionary entry
			
9) how to create list of dictionary/map from the predicted_rating_RDD , dictinary will have movie_id:movie_name as key and 
	rating_average as value
	
		for x in predicted_rating_RDD.collect():
            rating_average = sum(x[1])/len(x[1])
            dict = {"productId":str(x[0])+' : '+movie_dictionary[str(x[0])],"rating":rating_average}
            ratings.append(dict) # add the dictionary into the list

	