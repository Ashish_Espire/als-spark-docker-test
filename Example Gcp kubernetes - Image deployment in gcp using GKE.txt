Deploying a containerized web application in gcloud

Step 1: Build the container image

	A) Get the source code for a[plication

		git clone https://Ashish_Espire@bitbucket.org/Ashish_Espire/als-spark-docker-test.git
		cd als-spark-docker-test

	B) Set the PROJECT_ID environment variable to your Google Cloud project ID. This variable will be used to associate the
	 container image with your project's Container Registry.
	 
		export PROJECT_ID=[PROJECT_ID]
	 
	C) To build the container image of this application and tag it for uploading, run the following command:

		docker build -t gcr.io/${PROJECT_ID}/hello-app:v1 .

	Note : This command instructs Docker to build the image using the Dockerfile in the current directory and tag it with a name, 
	such as gcr.io/my-project/hello-app:v1. The gcr.io prefix refers to Google Container Registry, where the image will be hosted.
	Running this command does not upload the image yet.


Step 2: Upload the container image

	A) You need to upload the container image to a registry so that GKE can download and run it.
	First, configure Docker command-line tool to authenticate to Container Registry (you need to run this only once):
	
		gcloud auth configure-docker
		
	B)You can now use the Docker command-line tool to upload the image to your Container Registry:

		docker push gcr.io/${PROJECT_ID}/hello-app:v1
		
Step 3: Run your container locally (optional)

		docker run --rm -p 8080:8080 gcr.io/${PROJECT_ID}/hello-app:v1
	
	
Step 4: Create a container cluster

	Now that the container image is stored in a registry, you need to create a container cluster to run the container image. A 
	cluster consists of a pool of Compute Engine VM instances running Kubernetes, the open source cluster orchestration system
	that powers GKE. Once you have created a GKE cluster, you use Kubernetes to deploy applications to the cluster and manage
	the applications' lifecycle.

	A) Set your project ID and Compute Engine zone options for the gcloud tool:

		gcloud config set project $PROJECT_ID
		gcloud config set compute/zone [COMPUTE_ENGINE_ZONE]
		
	B) Run the following command to create a two-node cluster named hello-cluster:
	
		gcloud container clusters create hello-cluster --num-nodes=2
		
	C) run the following command and see the cluster's two worker VM instances:
	
		gcloud compute instances list
		
Note: If you are using an existing Google Kubernetes Engine cluster or if you have created a cluster through Google Cloud
 Console, you need to run the following command to retrieve cluster credentials and configure kubectl command-line tool with
 them: gcloud container clusters get-credentials hello-cluster
If you have already created a cluster with the gcloud container clusters create command listed above, this step is not 
necessary. 


Step 5: Deploy your application

	To deploy and manage applications on a GKE cluster, you must communicate with the Kubernetes cluster management system. You
	typically do this by using the kubectl command-line tool. Kubernetes represents applications as Pods, which are units that
	represent a container (or group of tightly-coupled containers). The Pod is the smallest deployable unit in Kubernetes. In 
	this tutorial, each Pod contains only your hello-app container.
	
	The kubectl create deployment command below causes Kubernetes to create a Deployment named hello-web on your cluster. 
	The Deployment manages multiple copies of your application, called replicas, and schedules them to run on the individual 
	nodes in your cluster. In this case, the Deployment will be running only one Pod of your application.

	A) Run the following command to deploy your application:
	
		kubectl create deployment hello-web --image=gcr.io/${PROJECT_ID}/hello-app:v1
	
	B) To see the Pod created by the Deployment, run the following command:	
	
		kubectl get pods
		
Step 6: Expose your application to the Internet

	By default, the containers you run on GKE are not accessible from the Internet, because they do not have external IP
	addresses. You must explicitly expose your application to traffic from the Internet.

	The kubectl expose command below creates a Service resource, which provides networking and IP support to your application's
	Pods. GKE creates an external IP and a Load Balancer (subject to billing) for your application.
	The --port flag specifies the port number configured on the Load Balancer, and the --target-port flag specifies the port
	number that the hello-app container is listening on.

    A) To expose the deployment over internet run the following command:

		kubectl expose deployment hello-web --type=LoadBalancer --port 80 --target-port 8080
		
	B) To see external ip of the loadbalancer run the following command :
	
		kubectl get service
		
Step 7: Scale up your application

	A )You add more replicas to your application's Deployment resource by using the kubectl scale command. To add two additional
	replicas to your Deployment (for a total of three), run the following command:
	
		kubectl scale deployment hello-web --replicas=3

	B) You can see the new replicas running on your cluster by running the following commands:

		kubectl get deployment hello-web
		
Step 8: Deploy a new version of your app

	GKE's rolling update mechanism ensures that your application remains up and available even as the system replaces instances
	of your old container image with your new one across all the running replicas. 
	You can create an image for the v2 version of your application by building the same source code and tagging it as v2
	(or you can change the "Hello, World!" string to "Hello, GKE!" before building the image):

	A) docker build -t gcr.io/${PROJECT_ID}/hello-app:v2 .

	B) Then push the image to the Google Container Registry:

		docker push gcr.io/${PROJECT_ID}/hello-app:v2

	
	C) Now, apply a rolling update to the existing deployment with an image update:

		kubectl set image deployment/hello-web hello-app=gcr.io/${PROJECT_ID}/hello-app:v2

Step 8 : Cleaning up

	 To avoid incurring charges to your Google Cloud Platform account for the resources used , follow these steps to remove the
	 following resources to prevent unwanted charges incurring on your account:

     A) Delete the Service: This step will deallocate the Cloud Load Balancer created for your Service:

		kubectl delete service hello-web

     B)Delete the container cluster: This step will delete the resources that make up the container cluster, such as the
	 compute instances, disks and network resources.
	 
		gcloud container clusters delete hello-cluster
		
		
Reference : https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app


=======================================================================================================================

Description :

Configure your Kubernetes Cluster

1) Create the Namespace :

Namespaces provide a powerful way to logically partition your Kubernetes cluster and isolate components and resources to avoid 
collisions across the cluster. A common use-case is to encapsulate dev/testing/production environments with namespaces so that 
they can each utilize the same resource names across each stage of development.

Namespaces add a layer of complexity to a cluster that may not always be necessary. It is important to keep this in mind when 
formulating the architecture for a project’s application. This example will create a namespace for demonstration purposes, but 
it is not a requirement. One situation where a namespace would be beneficial, in the context of this guide, would be if you were 
a developer and wanted to manage Hugo sites for several clients with a single Kubernetes cluster.

apiVersion: v1
kind: Namespace
metadata:
  name: hugo-site
  
# The key-value pair name: hugo-site defines the namespace object’s unique name.

kubectl create -f clientx/k8s-hugo/ns-hugo-site.yaml


2) Create the Service

The service will group together all pods for the Hugo site, expose the same port on all pods to the internet, and load balance 
site traffic between all pods. It is best to create a service prior to any controllers (like a deployment) so that the 
Kubernetes scheduler can distribute the pods for the service as they are created by the controller.

The Hugo site’s service manifest file will use the NodePort method to get external traffic to the Hugo site service. NodePort 
opens a specific port on all the Nodes and any traffic that is sent to this port is forwarded to the service. Kubernetes will 
choose the port to open on the nodes if you do not provide one in your service manifest file. It is recommended to let 
Kubernetes handle the assignment. Kubernetes will choose a port in the default range, 30000-32767.

apiVersion: v1
kind: Service
metadata:
  name: hugo-site
  namespace: hugo-site
spec:
  selector:
    app: hugo-site
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
  type: NodePort
  
      The spec key defines the Hugo site service object’s desired behavior. It will create a service that exposes TCP port 80
	  on any pod with the app: hugo-site label.
    The exposed container port is defined by the targetPort:80 key-value pair.
	
kubectl create -f clientx/k8s-hugo/service-hugo.yaml

3) Create the Deployment

A deployment is a controller that helps manage the state of your pods. The Hugo site deployment will define how many pods
 should be kept up and running with the Hugo site service and which container image should be used.
 
kubectl create -f clientx/k8s-hugo/deployment.yaml
 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hugo-site
  namespace: hugo-site
spec:
  replicas: 3
  selector:
    matchLabels:
      app: hugo-site
  template:
    metadata:
      labels:
        app: hugo-site
    spec:
      containers:
      - name: hugo-site
        image: mydockerhubusername/hugo-site:v1
        imagePullPolicy: Always
        ports:
        - containerPort: 80
		
The deployment’s object spec states that the deployment should have 3 replica pods. This means at any given time the cluster 
will have 3 pods that run the Hugo site service. The template field provides all the information needed to create actual pods.
The label app: hugo-site helps the deployment know which service pods to target. The container field states that any 
containers connected to this deployment should use the Hugo site image mydockerhubusername/hugo-site:v1 
imagePullPolicy: Always means that the container image will be pulled every time the pod is started.
containerPort: 80 states the port number to expose on the pod’s IP address. The system does not rely on this field to expose
the container port, instead, it provides information about the network connections a container uses.

		

		



	

		


		
	